# Angular Tech Test

## Introduction

This is a sample application to test front end skills.

Refer to the screenshots here: [https://imgur.com/a/15cefvw](https://imgur.com/a/15cefvw)

The goal of the application is to provide the user with a search form, which given a specific
term, will go on the Wikipedia web site and extract the **top-level** table of contents.

## Instructions

There are a few things left to implement for the application to be complete:

1. **The content extractor.** The `WikipediaService` class currently calls the API for the
requested page. The part that is left to implement is to extract the relevant information from the
response body. (screenshot #2)

2. **Routing.** The page that displays the search result must be accessible using a permalink, in
this case `/details/LANGUAGE/PAGE_NAME`. (screenshot #2)

3. **Error handling.** If the page does not exist or the request otherwise gives an error,
a message must be shown to the user. (screenshot #3)

4. **Styling.** The buttons must be styled and the content must be centered on the page, according
to the screenshots. Resizing the browser window should make the content flow so that it's kept at
the center of the viewport, both vertically and horizontally. (screenshots #1, #2 and #3)

