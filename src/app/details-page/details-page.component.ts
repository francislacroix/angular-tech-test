import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { WikipediaService } from '../wikipedia.service';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {

  public language: string;
  public query: string;
  public sections: string[];
  public isError = false;

  constructor(
    route: ActivatedRoute,
    private router: Router,
    private wikiService: WikipediaService,
  ) {
    route.params.subscribe((params) => {
      this.language = params.language;
      this.query = params.query;
    })
  }

  async ngOnInit() {
    try {
      const data = await this.wikiService.getTableOfContents(this.language, this.query);
      this.sections = data.items;
    } catch (err) {

    }
  }

  public goBack(): void {
    this.router.navigate(['']);
  }

}
