import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {

  public language = 'en';
  public query = '';
  public showError = false;

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }

  public submit(): void {
    if (this.query.trim() === '') {
      this.showError = true;
    } else {
      this.showError = false;
      const url = `details/${this.language}/${this.query}`;
      this.router.navigate([url]);
    }
  }

}
