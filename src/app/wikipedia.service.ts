import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface WikiResult {
  rawContent: string;
  items: string[];
}

@Injectable({
  providedIn: 'root'
})
export class WikipediaService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  private getPageContent(language: string, name: string): Promise<any> {
    const page = encodeURIComponent(name);
    const url = `https://${language}.wikipedia.org/w/api.php?origin=*&format=json&action=parse&page=${page}`;

    return new Promise((resolve) => {
      this.httpClient.get(url).subscribe(
        (result) => {
          resolve(result);
        },
        (err) => {
          console.error(err);
          throw new Error('there was an error');
        },
      );
    });
  }

  public async getTableOfContents(language: string, name: string): Promise<WikiResult> {
    const rawContent = await this.getPageContent(language, name);
    const items = this.extractTOC(rawContent);

    return {
      rawContent: rawContent,
      items: items,
    }
  }

}
